package kz.astana.listexamples;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(new ListItems().getRecyclerListItems());
        recyclerView.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setOnClickListener(new RecyclerAdapter.MyClickListener() {
            @Override
            public void onClick(int position) {
                recyclerAdapter.removeItem(position);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerAdapter.addItem(new Country(R.drawable.ic_afghanistan, "Afghanistan", "Capital"));
            }
        });
    }
}