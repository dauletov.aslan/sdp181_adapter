package kz.astana.listexamples;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    private ArrayList<Country> data;
    private MyClickListener myClickListener;

    public RecyclerAdapter(ArrayList<Country> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_country, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        Country country = data.get(position);

        holder.flagRedId.setImageResource(country.getFlagResId());
        holder.countryName.setText(country.getCountryName());
        holder.countryCapital.setText(country.getCountryCapital());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addItem(Country country) {
        data.add(country);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyDataSetChanged();
    }

    public void setOnClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public ImageView flagRedId;
        public TextView countryName, countryCapital;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            flagRedId = itemView.findViewById(R.id.countryFlagImageView);
            countryName = itemView.findViewById(R.id.countryNameTextView);
            countryCapital = itemView.findViewById(R.id.countryCapitalTextView);
        }
    }

    public interface MyClickListener {
        void onClick(int position);
    }
}
