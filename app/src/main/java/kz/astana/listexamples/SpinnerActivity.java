package kz.astana.listexamples;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SpinnerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                SpinnerActivity.this,
                android.R.layout.simple_spinner_item,
                new ListItems().getListItems()
        );
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(SpinnerActivity.this, adapter.getItem(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button selItem = findViewById(R.id.selectedItem);
        selItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(
                        SpinnerActivity.this,
                        spinner.getSelectedItem().toString(),
                        Toast.LENGTH_SHORT
                )
                        .show();
            }
        });

        Button selItemPos = findViewById(R.id.selectedItemPosition);
        selItemPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(
                        SpinnerActivity.this,
                        spinner.getSelectedItemPosition() + "",
                        Toast.LENGTH_SHORT
                )
                        .show();
            }
        });
    }
}