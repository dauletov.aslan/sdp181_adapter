package kz.astana.listexamples;

public class Country {

    private int flagResId;
    private String countryName;
    private String countryCapital;

    public Country(int flagResId, String countryName, String countryCapital) {
        this.flagResId = flagResId;
        this.countryName = countryName;
        this.countryCapital = countryCapital;
    }

    public int getFlagResId() {
        return flagResId;
    }

    public void setFlagResId(int flagResId) {
        this.flagResId = flagResId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCapital() {
        return countryCapital;
    }

    public void setCountryCapital(String countryCapital) {
        this.countryCapital = countryCapital;
    }
}
