package kz.astana.listexamples;

import java.util.ArrayList;

public class ListItems {

    public ArrayList<String> getListItems() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("Kazakhstan");
        list.add("China");
        list.add("Russia");
        list.add("Kyrgyzstan");
        list.add("Germany");
        list.add("Korea");
        list.add("Japan");
        list.add("Iran");
        list.add("Iraq");
        list.add("Turkey");
        list.add("England");
        list.add("Scotland");
        list.add("Spain");

        return list;
    }

    public ArrayList<Country> getRecyclerListItems() {
        ArrayList<Country> list = new ArrayList<Country>();
        list.add(new Country(R.drawable.ic_abkhazia, "Kazakhstan", "Nur-Sultan"));
        list.add(new Country(R.drawable.ic_abkhazia, "China", "Beijing"));
        list.add(new Country(R.drawable.ic_abkhazia, "Russia", "Moscow"));
        list.add(new Country(R.drawable.ic_abkhazia, "Kyrgyzstan", "Bishkek"));
        list.add(new Country(R.drawable.ic_abkhazia, "Germany", "Berlin"));
        list.add(new Country(R.drawable.ic_abkhazia, "Korea", "Seoul"));
        list.add(new Country(R.drawable.ic_abkhazia, "Japan", "Tokyo"));
        list.add(new Country(R.drawable.ic_abkhazia, "Iran", "Tegeran"));
        list.add(new Country(R.drawable.ic_abkhazia, "Iraq", "Baqdad"));
        list.add(new Country(R.drawable.ic_abkhazia, "Turkey", "Ankara"));
        list.add(new Country(R.drawable.ic_abkhazia, "England", "London"));
        list.add(new Country(R.drawable.ic_abkhazia, "Scotland", "Edinburg"));
        list.add(new Country(R.drawable.ic_abkhazia, "Spain", "Madrid"));

        return list;
    }
}
